package words;

import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class WordsCounterTest {
    private WordsCounter counter = new WordsCounter();

    @Test
    public void counterTest() throws IOException  {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("words.txt").getFile());
        Properties props = new Properties();
        props.load(new FileReader(new File(classLoader.getResource("words-counts.txt").getFile())));

        Map<String, Integer> words = counter.countWords(file.getAbsolutePath());

        assertEquals(props.size(), words.size());
        props.forEach((k, v) -> {
            assertEquals(new Integer(v.toString()), words.get(k.toString()));
        });
    }
}
