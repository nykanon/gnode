package gnode;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GNodeChallengeTest {
    private GNodeChallenge codeChallenge = new GNodeChallenge();

    @Test
    public void walkGraphNoChildrenTest() {
        ArrayList<GNode> list = codeChallenge.walkGraph(new GNodeImpl("Test"));
        assertEquals(1, list.size());
        assertEquals("Test", list.get(0).getName());
    }

    @Test
    public void walkGraphTest() {
        List<String> expectedNames = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I");
        ArrayList<GNode> list = codeChallenge.walkGraph(getData());
        assertEquals(expectedNames.size(), list.size());
        List<String> names = list.stream().map(GNode::getName).collect(Collectors.toList());
        assertTrue(names.containsAll(expectedNames));
    }

    @Test
    public void pathsNoChildrenTest() {
        ArrayList<ArrayList<GNode>> list = codeChallenge.paths(new GNodeImpl("Test"));
        assertEquals(1, list.size());
        ArrayList<GNode> path = list.get(0);
        assertEquals(1, path.size());
        assertEquals("Test", path.get(0).getName());
    }

    @Test
    public void pathsTest() {
        ArrayList<GNode> list = codeChallenge.walkGraph(getData());
        for(GNode n : list) {
            ArrayList<ArrayList<GNode>> paths = codeChallenge.paths(n);
            List<List<String>> expectedPaths = getPaths(n.getName());
            assertEquals(expectedPaths.size(), paths.size());
            for(int i = 0; i < expectedPaths.size(); i++) {
                List<String> path = paths.get(i).stream().map(GNode::getName).collect(Collectors.toList());
                assertTrue(expectedPaths.get(i).containsAll(path));
            }
        }
    }

    private List<List<String>> getPaths(String name) {
        if ("A".equals(name)) {
            return Arrays.asList(
                    Arrays.asList("A", "B", "E"),
                    Arrays.asList("A", "B", "F"),
                    Arrays.asList("A", "C", "G"),
                    Arrays.asList("A", "C", "H"),
                    Arrays.asList("A", "C", "I"),
                    Arrays.asList("A", "D")
            );
        } else if ("B".equals(name)) {
            return Arrays.asList(Arrays.asList("B", "E"), Arrays.asList("B", "F"));
        } else if ("C".equals(name)) {
            return Arrays.asList(Arrays.asList("C", "G"), Arrays.asList("C", "H"), Arrays.asList("C", "I"));
        } else {
            return Collections.singletonList(Collections.singletonList(name));
        }
    }

    private GNode getData() {
        GNodeImpl a = new GNodeImpl("A");
        GNodeImpl b = new GNodeImpl("B");
        GNodeImpl c = new GNodeImpl("C");

        a.addNode(b);
        a.addNode(c);
        a.addNode(new GNodeImpl("D"));

        b.addNode(new GNodeImpl("E"));
        b.addNode(new GNodeImpl("F"));

        c.addNode(new GNodeImpl("G"));
        c.addNode(new GNodeImpl("H"));
        c.addNode(new GNodeImpl("I"));

        return a;
    }
}
