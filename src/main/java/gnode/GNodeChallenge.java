package gnode;

import java.util.ArrayList;
import java.util.Collections;

public class GNodeChallenge {
    public ArrayList<GNode> walkGraph(GNode node) {
        ArrayList<GNode> result = new ArrayList<>();
        result.add(node);
        for(GNode n : node.getChildren()) {
            result.addAll(walkGraph(n));
        }
        return result;
    }

    public ArrayList<ArrayList<GNode>> paths(GNode node) {
        ArrayList<ArrayList<GNode>> result = new ArrayList<>();
        if (node.getChildren().length > 0) {
            for(GNode n : node.getChildren()) {
                ArrayList<ArrayList<GNode>> nodePaths = paths(n);
                for (ArrayList<GNode> nodePath : nodePaths) {
                    nodePath.add(0, node);
                    result.add(nodePath);
                }
            }
        } else {
            result.add(new ArrayList<>(Collections.singletonList(node)));
        }
        return result;
    }
}
