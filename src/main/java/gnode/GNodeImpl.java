package gnode;

import java.util.ArrayList;

public class GNodeImpl implements GNode {
    private String name;
    private ArrayList<GNode> children;

    public GNodeImpl(String name) {
        this.name = name;
        this.children = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public GNode[] getChildren() {
        return children.toArray(new GNode[0]);
    }

    public void addNode(GNode node) {
        children.add(node);
    }
}
