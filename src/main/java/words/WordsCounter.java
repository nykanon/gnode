package words;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class WordsCounter {
    public Map<String, Integer> countWords(String fileName) {
        Map<String, Integer> result = new HashMap<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(line -> {
                for (String word : line.toLowerCase().replaceAll("[^a-z]", " ").split(" ")) {
                    if (word.length() > 0) {
                        result.compute(word, (k, v) -> (v == null) ? 1 : v + 1);
                    }
                }
            });
        } catch (IOException e) {
            System.err.println("Couldn't read file: " + fileName + ". Error: " + e.getMessage());
        }
        return result;
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("No file name specified");
        } else {
            new WordsCounter().countWords(args[0]).forEach((word, count) -> System.out.println(count + " " + word));
        }
    }
}
