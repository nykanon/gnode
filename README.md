GNode and Word Count
--------------------

**Code building using gradle:**

`./gradlew build`

run tests only:

`./gradlew test`

start the word count application:

`java -jar build/libs/gnode-1.0-SNAPSHOT.jar src/test/resources/words.txt`
